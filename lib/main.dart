import 'package:astro_app/log_in.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_indicator/page_indicator.dart';
import 'daily_horoscope.dart';
import 'model/chat_with_astrologer.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => MyHomePage(),
        '/dailyHoroscope': (context) => DailyHoroscopePage(),
        '/chatWithAstrologer': (context) => AstrologerChatPage(),
        '/loginPage': (context) => LoginPage()
      },
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
//      home: MyHomePage(title: 'Astro App'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Astro Magica'),
          actions: [
            Center(
              child: Row(
                children: [
                  Container(
                      child: Row(
                        children: [
                          Icon(
                            Icons.account_balance_wallet,
                            size: 18,
                          ),
                          Text(
                            ' ₹0',
                            style: TextStyle(
                                fontWeight: FontWeight.w400, fontSize: 15.0),
                          )
                        ],
                      ),
                      padding: EdgeInsets.fromLTRB(5, 3, 5, 3),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black, width: 1),
                          borderRadius: BorderRadius.circular(5))),
                  Container(
                    margin: EdgeInsets.fromLTRB(20, 0, 15, 0),
                    child: Icon(Icons.person),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 0, 15, 0),
                    child: Icon(Icons.notifications_none),
                  )
                ],
              ),
            )
          ],
        ),
        body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/bg_feather.jpg"),
                  fit: BoxFit.cover
              )
          ),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                BannerAddPageView(),
                ZodHorListView(),
                FeatureGridLayout(),
                OrderHistoryLayout(),
                AstroAppNewsWidget(),
                BehindTheSceneWidget(),
                BottomToolsWidget()
              ],
            ),
          ),
        ),

        drawer: Container(
          width: 350,
          child: Drawer(
            // Add a ListView to the drawer. This ensures the user can scroll
            // through the options in the drawer if there isn't enough vertical
            // space to fit everything.
            child: ListView(
              // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: <Widget>[
                Container(
                  width: 50,
                  height: 20,
                ),
                Container(
                  width: 100,
                  height: 100,
                  margin: EdgeInsets.all(30),
                  child: Image.asset(
                    'assets/images/zodiac12.png', fit: BoxFit.scaleDown,),
                ),
                Divider(
                  height: 1.0,
                  color: Colors.black26,
                  thickness: 1.0,
                ),
                ListTile(
                  leading: Icon(
                    Icons.attach_money,
                    color: Colors.black,
                  ),
                  title: Text('Wallet Transactions'),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                  },
                ),
                ListTile(
                  leading: Icon(Icons.sort_by_alpha, color: Colors.black),
                  title: Text('Order History'),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                  },
                ),
                ListTile(
                  leading: Image.asset('assets/images/support_black_24.png'),
                  title: Text('Customer Support Chat'),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                  },
                ),
                Container(
                  height: 50,
                  alignment: Alignment.center,
                  child: Row(
                    children: [
                      Expanded(
                        child: Flex(
                            direction: Axis.horizontal,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(18, 0, 18, 0),
                                child: Image.asset('assets/images/free.png'),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                child: Text(
                                  'Free Services',
                                  style: TextStyle(color: Colors.black),
                                ),
                              ),
                            ]),
                        flex: 1,
                      ),
                      Expanded(
                        child: Flex(
                            direction: Axis.horizontal,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 18, 0),
                                child: Image.asset('assets/images/blog.png'),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                child: Text(
                                  'Astro Blog',
                                  style: TextStyle(color: Colors.black),
                                ),
                              ),
                            ]),
                        flex: 1,
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 50,
                  alignment: Alignment.center,
                  child: Row(
                    children: [
                      Expanded(
                        child: Flex(
                            direction: Axis.horizontal,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(18, 0, 18, 0),
                                child: Image.asset(
                                    'assets/images/instructions.png'),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                child: Text(
                                  'How to use',
                                  style: TextStyle(color: Colors.black),
                                ),
                              ),
                            ]),
                        flex: 1,
                      ),
                      Expanded(
                        child: Flex(
                            direction: Axis.horizontal,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 18, 0),
                                child: Image.asset('assets/images/info_24.png'),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                child: Text(
                                  'About us',
                                  style: TextStyle(color: Colors.black),
                                ),
                              ),
                            ]),
                        flex: 1,
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 50,
                  alignment: Alignment.center,
                  child: Row(
                    children: [
                      Expanded(
                        child: Flex(
                            direction: Axis.horizontal,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(18, 0, 18, 0),
                                child: Image.asset('assets/images/review.png'),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                child: Text(
                                  'Rate Us',
                                  style: TextStyle(color: Colors.black),
                                ),
                              ),
                            ]),
                        flex: 1,
                      ),
                      Expanded(
                        child: Flex(
                            direction: Axis.horizontal,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 18, 0),
                                child: Image.asset('assets/images/share.png'),
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                child: Text(
                                  'Share',
                                  style: TextStyle(color: Colors.black),
                                ),
                              ),
                            ]),
                        flex: 1,
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 50,
                  alignment: Alignment.center,
                  child: Row(
                    children: [
                      Expanded(
                        child: Flex(
                            direction: Axis.horizontal,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(18, 0, 0, 0),
                                child: Image.asset('assets/images/login.png'),
                              ),
                              GestureDetector(
                                onTap: (){
                                  goToLoginRoute(context);
                                },
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(18, 10, 18, 10),
                                  child: Text(
                                    'Login',
                                    style: TextStyle(color: Colors.black),
                                  ),
                                ),
                              ),
                            ]),
                        flex: 1,
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Align(
                    child: Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      height: 15,
                      child: Text(
                        'Also available on:',
                        style: TextStyle(
                          fontSize: 10,
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 7, 0, 0),
                  child: Flex(
                    direction: Axis.horizontal,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Image.asset('assets/images/apple_logo.png'),
                      Image.asset('assets/images/internet.png'),
                      Image.asset('assets/images/pinterest.png'),
                      Image.asset('assets/images/fb_2.png'),
                      Image.asset('assets/images/instagram.png'),
                      Image.asset('assets/images/twitter.png'),
                      Image.asset('assets/images/blog.png'),
                    ],
                  ),
                ),
                Expanded(
                  child: Align(
                    child: Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      height: 15,
                      child: Text(
                        'Follow us & we will follow you back :)',
                        style: TextStyle(
                          fontSize: 10,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  void goToLoginRoute(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => LoginPage()));
  }
}

class BannerAddPageView extends StatelessWidget {
  final controller = PageController(initialPage: 1);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
      margin: EdgeInsets.all(8),
      child: SizedBox(
        height: 200.0,
        child: PageIndicatorContainer(
          length: 4,
          align: IndicatorAlign.bottom,
          indicatorSpace: 5.0,
          padding: const EdgeInsets.all(10),
          indicatorColor: Colors.white,
          indicatorSelectorColor: Colors.black54,
          shape: IndicatorShape.circle(size: 8),
          child: PageView(
            controller: controller,
            onPageChanged: (page) => {print(page.toString())},
            pageSnapping: true,
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              Container(
                child: Center(
                  child: Image.asset(
                    "assets/images/astrology.jpg",
                  ),
                ),
              ),
              Container(
                child: Image.asset("assets/images/ganesha.jpg"),
//                      color: Colors.red,
              ),
              Container(
                child: Image.asset("assets/images/tarot.jpg"),
//                      color: Colors.red,
              ),
              Container(
                child: Image.asset("assets/images/kundli.jpg"),
//                      color: Colors.red,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ZodHorListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8.0),
      height: 100.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          ZodiacCards('Aries', "assets/images/goat.png"),
          ZodiacCards('Taurus', "assets/images/taurus.png"),
          ZodiacCards('Gemini', "assets/images/gemini.png"),
          ZodiacCards('Cancer', "assets/images/cancer.png"),
          ZodiacCards('Leo', "assets/images/lion.png"),
          ZodiacCards('Libra', "assets/images/goat.png"),
          ZodiacCards('Scorpio', "assets/images/scorpion.png"),
          ZodiacCards('Sagittarius', "assets/images/goat.png"),
          ZodiacCards('Capricorn', "assets/images/goat.png"),
          ZodiacCards('Aquarius', "assets/images/aquarius.png"),
          ZodiacCards('Pisces', "assets/images/zodiac.png"),
        ],
      ),
    );
  }
}

class ZodiacCards extends StatelessWidget {
  var _zodiacName;
  var _zodiacIconName;

  ZodiacCards(this._zodiacName, this._zodiacIconName);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: GestureDetector(
          onTap: () {
            print("card clicked $_zodiacName ");
            _onZodiacTapped(context, _zodiacName, _zodiacIconName);
          },
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Center(
                    child: Image.asset(_zodiacIconName, scale: 15,),
                  ),
                  Center(
                    child: Text(
                      _zodiacName,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Center(
                    child: Text('21 Mar - 19 Apr'),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}

void _onZodiacTapped(context, zodiacName, zodiacIcon) {
  Navigator.pushNamed(context, '/dailyHoroscope');
}

void _onTileClicked(BuildContext context, int i) {
  Navigator.of(context)
      .push(MaterialPageRoute(builder: (context) => AstrologerChatPage()));
}

class FeatureGridLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Container(
                  height: 170.0,
                  child: GestureDetector(
                    child: Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12)),
                      child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              child: Icon(
                                Icons.chat,
                                size: 70,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                              child: Text(
                                'Chat with Astrologer',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            Divider(
                              thickness: 0.5,
                            ),
                            Container(
                              child: Text(
                                'Starting from ₹5/min only',
                                style: TextStyle(fontSize: 10),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.all(5),
                              child: Text(
                                '(First minute is not charged)',
                                style: TextStyle(fontSize: 8),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    onTap: () {
                      _onTileClicked(context, 1);
                    },
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: 170.0,
                  child: GestureDetector(
                    child: Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0)),
                      child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Center(
                              child: Icon(
                                Icons.phone_in_talk,
                                size: 70,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                              child: Text(
                                'Talk to Astrologer',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            Divider(
                              thickness: 0.5,
                            ),
                            Container(
                              child: Text(
                                'Starting from ₹5/min only',
                                style: TextStyle(fontSize: 10),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.all(5),
                              child: Text(
                                '(First minute is not charged)',
                                style: TextStyle(fontSize: 8),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    onTap: () {
                      _onTileClicked(context, 2);
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Container(
                  height: 170.0,
                  child: GestureDetector(
                    child: Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0)),
                      child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Center(
                              child: Icon(
                                Icons.receipt,
                                size: 70,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                              child: Text(
                                'Get Detailed Report',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            Divider(
                              thickness: 0.5,
                            ),
                            Container(
                              child: Text(
                                'Starting from ₹300 only',
                                style: TextStyle(fontSize: 10),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.all(5),
                              child: Text(
                                '(Manually prepared by astrologer)',
                                style: TextStyle(fontSize: 8),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    onTap: () {
                      _onTileClicked(context, 3);
                    },
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: 170.0,
                  child: GestureDetector(
                    child: Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0)),
                      child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Center(
                              child: Icon(
                                Icons.local_mall,
                                size: 70,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                              child: Text(
                                'Shop at AstroShop',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            Divider(
                              thickness: 0.5,
                            ),
                            Container(
                              child: Text(
                                'Starting from ₹499 only',
                                style: TextStyle(fontSize: 10),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.all(5),
                              child: Text(
                                '(Get free callback request)',
                                style: TextStyle(fontSize: 8),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    onTap: () {
                      _onTileClicked(context, 4);
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class OrderHistoryLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5.0),
      child: Card(
        shape:
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                Expanded(
                  child: Container(
                    margin: EdgeInsets.fromLTRB(16, 16, 0, 8),
                    child: Text(
                      'Order History',
                      style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.fromLTRB(3, 3, 3, 10),
              child: Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      child: Container(
                        margin: EdgeInsets.all(5),
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(color: Colors.grey, spreadRadius: 1)
                              ]),
                          child: Container(
                            padding: EdgeInsets.all(8),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.call,
                                  size: 30,
                                ),
                                Text(
                                  'Call',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      onTap: () {
                        _onTileClicked(context, 1);
                      },
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      child: Container(
                        margin: EdgeInsets.all(5),
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(color: Colors.grey, spreadRadius: 1)
                              ]),
                          child: Container(
                            padding: EdgeInsets.all(8),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.chat,
                                  size: 30,
                                ),
                                Text(
                                  'Chat',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      onTap: () {
                        _onTileClicked(context, 2);
                      },
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      child: Container(
                        margin: EdgeInsets.all(5),
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(color: Colors.grey, spreadRadius: 1)
                              ]),
                          child: Container(
                            padding: EdgeInsets.all(8),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.receipt,
                                  size: 30,
                                ),
                                Text(
                                  'Report',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      onTap: () {
                        _onTileClicked(context, 3);
                      },
                      behavior: HitTestBehavior.translucent,
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      child: Container(
                        margin: EdgeInsets.all(5),
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(color: Colors.grey, spreadRadius: 1)
                              ]),
                          child: Container(
                            padding: EdgeInsets.all(8),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.local_mall,
                                  size: 30,
                                ),
                                Text(
                                  'Astro Mall',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      onTap: () {
                        _onTileClicked(context, 4);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BehindTheSceneWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(5, 3, 5, 10),
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(12, 12, 5, 0),
                  child: Text("Behind the Scene",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20
                    ),
                  ),
                ),

              ],),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  flex: 1,
                  child: Container(
                    margin: EdgeInsets.fromLTRB(20, 12, 5, 12),
                    width: 180,
                    height: 180,
                    decoration: BoxDecoration(
                        color: Colors.green,
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(8)
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Image.asset(
                            "assets/images/read.png", scale: 1.5,),
                        ),
                        Container(
                          child: Text("Our Story",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 24
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(35, 8, 35, 8),
                          margin: EdgeInsets.fromLTRB(0, 5, 0, 18),
                          decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.circular(5)
                          ),
                          child: Text("Read",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 18
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    margin: EdgeInsets.fromLTRB(5, 12, 20, 12),
                    width: 180,
                    height: 180,
                    decoration: BoxDecoration(
                        color: Colors.red,
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(8)
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Image.asset(
                            "assets/images/video.png", scale: 1.5,),
                        ),
                        Container(
                          child: Text("Our Story",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 24
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(35, 8, 35, 8),
                          margin: EdgeInsets.fromLTRB(0, 5, 0, 18),
                          decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.circular(5)
                          ),
                          child: Text("Watch",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 18
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class AstroAppNewsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10)
        ),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(15, 15, 0, 10),
                  child: Text("AstroApp in News",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20
                    ),),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.fromLTRB(15, 0, 15, 10),
              child: SizedBox(
                height: 120,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: 7,
                  itemBuilder: (context, position){
                    return Container(
                      margin: EdgeInsets.all(5),
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(color: Colors.grey, spreadRadius: 2)
                            ]),
                        child: Container(
                          padding: EdgeInsets.fromLTRB(15,0,15,0),
                          child: Center(
                            child: Text(
                              'YOUR STORY',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red,
                                  fontSize: 30
                              ),
                            ),
                          )
                        ),
                      ),
                    );
                  },

                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BottomToolsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(

      color: Colors.white,
      child: Column(
        children: [
          Container(color: Colors.black, height: 1.5),

         Row(
           children: [
             Container(
               height: 55,
               child: Column(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: [
                   Expanded(
                       child: Container(
                         color: Colors.white,
                         child: Row(
                           mainAxisAlignment: MainAxisAlignment.center,
                           crossAxisAlignment: CrossAxisAlignment.center,
                           children: [
                             Container(
                               margin: EdgeInsets.fromLTRB(12, 0, 12, 0),
                               child: Text(
                                 "PRIVATE &\nCONFIDENTIAL",
                                 textAlign: TextAlign.center,
                                 style: TextStyle(
                                     color: Colors.black
                                 ),
                               ),
                             ),
                             VerticalDivider(color: Colors.black, thickness: 2,)
                           ],
                         ),
                       )
                   ),
                 ],
               ),
             ),
             Container(
               height: 55,
               child: Column(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: [
                   Expanded(
                       child: Container(
                         color: Colors.white,
                         child: Row(
                           mainAxisAlignment: MainAxisAlignment.center,
                           crossAxisAlignment: CrossAxisAlignment.center,
                           children: [
                             Container(
                               margin: EdgeInsets.fromLTRB(12, 0, 12, 0),
                               child: Text(
                                 "VERIFIED\nASTROLOGERS",
                                 textAlign: TextAlign.center,
                                 style: TextStyle(
                                     color: Colors.black
                                 ),
                               ),
                             ),
                             VerticalDivider(color: Colors.black, thickness: 2,)
                           ],
                         ),
                       )
                   ),
                 ],
               ),
             ),
             Container(
               height: 55,
               child: Column(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: [
                   Expanded(
                       child: Container(
                         color: Colors.white,
                         child: Row(
                           mainAxisAlignment: MainAxisAlignment.center,
                           crossAxisAlignment: CrossAxisAlignment.center,
                           children: [
                             Container(
                               margin: EdgeInsets.fromLTRB(12, 0, 12, 0),
                               child: Text(
                                 "100% SECURE\nPAYMENT",
                                 textAlign: TextAlign.center,
                                 style: TextStyle(
                                     color: Colors.black
                                 ),
                               ),
                             ),
                           ],
                         ),
                       ),
                   ),
                 ],
               ),
             ),

           ],
         )

        ],
      )
    );
  }
}
